<?php

namespace App\DataFixtures;

use App\Entity\Merchant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MerchantFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
		$merchant = new Merchant();
		$merchant->setName('ABC Manufacturer');
	    $this->addReference('merchant_1', $merchant);
		$manager->persist($merchant);
	
	    $merchant = new Merchant();
	    $merchant->setName('XYZ Manufacturer');
	    $this->addReference('merchant_2', $merchant);
	    $manager->persist($merchant);

        $manager->flush();
    }
}
