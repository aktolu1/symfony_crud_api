<?php

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ClientFixtures extends Fixture
{
	private $passwordHasher;
	
	public function __construct(UserPasswordHasherInterface $passwordHasher)
	{
		$this->passwordHasher = $passwordHasher;
	}
	
    public function load(ObjectManager $manager): void
    {
		$client = new Client();
	    $client->setRoles(['ROLE_USER']);
		$client->setName('Client 1');
		$client->setUsername('client_1');
		$client->setPassword($this->passwordHasher->hashPassword($client, '1234'));
		$manager->persist($client);
	
	    $client = new Client();
	    $client->setRoles(['ROLE_USER']);
	    $client->setName('Client 2');
	    $client->setUsername('client_2');
	    $client->setPassword($this->passwordHasher->hashPassword($client, '1234'));
	    $manager->persist($client);
	
	    $client = new Client();
	    $client->setRoles(['ROLE_USER']);
	    $client->setName('Client 3');
	    $client->setUsername('client_3');
	    $client->setPassword($this->passwordHasher->hashPassword($client, '1234'));
	    $manager->persist($client);

        $manager->flush();
    }
}
