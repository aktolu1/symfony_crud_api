<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
		$product = new Product();
		$product->setName('Iphone 12 Pro');
		$product->setMerchant($this->getReference('merchant_1'));
		$manager->persist($product);
	
	    $product = new Product();
	    $product->setName('Monster Notebook');
	    $product->setMerchant($this->getReference('merchant_1'));
	    $manager->persist($product);
	
	    $product = new Product();
	    $product->setName('127 inch TV');
	    $product->setMerchant($this->getReference('merchant_2'));
	    $manager->persist($product);
	
	    $product = new Product();
	    $product->setName('Keyboard');
	    $product->setMerchant($this->getReference('merchant_2'));
	    $manager->persist($product);

        $manager->flush();
    }
}
