<?php

namespace App\Controller\Api;

use App\Entity\Client;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

class OrderController extends AbstractApiController
{
	private EntityManagerInterface $em;
	
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}
	
	
    #[Route('/api/orders', name: 'app_api_orders', methods: ['GET'])]
    #[OA\Response(
	    response: 200,
	    description: 'List all orders',
	    content: new Model(type: Order::class)
    )]
    public function index(): Response
    {
	    $request = Request::createFromGlobals();
	
	    $client = $this->decodeJWT($request, $this->em);
		
		$repository = $this->em->getRepository(Order::class);
		
		$items = $repository->findBy(['client' => $client]);
		
		return $this->respond($items, 'Success');
    }
	
	#[Route('/api/orders/{id}', name: 'app_api_orders_get', defaults: ['id' => null], methods: ['GET'])]
	public function get($id): Response
	{
		$request = Request::createFromGlobals();
		
		$client = $this->decodeJWT($request, $this->em);
		
		$repository = $this->em->getRepository(Order::class);
		
		$items = $repository->findOneBy([
			'id' => $id,
			'client' => $client,
		]);
		
		if (!$items) {
			return $this->respond([
				'orderId' => $id,
			], Response::HTTP_BAD_REQUEST);
		}
		
		return $this->respond($items, 'Success');
	}
	
	#[Route('/api/orders/add', name: 'app_api_orders_add', methods: ['POST'])]
	public function add(ManagerRegistry $doctrine, Request $request): Response
	{
		$client = $this->decodeJWT($request, $this->em);
		$entityManager = $doctrine->getManager();
		$productRepository = $this->em->getRepository(Product::class);
		
		$data = new Order();
		$data->setQuantity($request->request->get('quantity'));
		$data->setAddress($request->request->get('address'));
		
		$data->setOrderCode('ORDER-'.rand(100000, 999999));
		
		$data->setClient($client);
		
		
		$product = $productRepository->findAll();
		shuffle($product);
		$data->setProduct($product[0]);
		
		$entityManager->persist($data);
		$entityManager->flush();
		
		$return = [
			'id' => $data->getId(),
			'orderCode' => $data->getOrderCode(),
			'product' => $data->getProduct()->getName(),
			'quantity' => $data->getQuantity(),
			'address' => $data->getAddress(),
			'client' => $data->getClient()->getName(),
		];
		
		return $this->respond($return, 'Success');
	}
	
	#[Route('/api/orders/update/{id}', name: 'app_api_orders_update', methods: ['POST'])]
	public function update($id, Request $request): Response
	{
		$client = $this->decodeJWT($request, $this->em);
		$repository = $this->em->getRepository(Order::class);
		$productRepository = $this->em->getRepository(Product::class);
		
		$item = $repository->findOneBy([
			'id' => $id,
			'client' => $client,
		]);
		if (!$item) {
			return $this->respond($request->request->all(), 'Order not found!', Response::HTTP_BAD_REQUEST);
		}
		
		if (!empty($item->getShippingDate())) {
			return $this->respond($request->request->all(), 'Order is already shipped!', Response::HTTP_BAD_REQUEST);
		}
		
		$product = $productRepository->find($request->request->get('product'));
		if (!$product) {
			return $this->respond($request->request->all(), 'Product not found!', Response::HTTP_BAD_REQUEST);
		}
		
		$item->setProduct($product);
		
		$item->setQuantity($request->request->get('quantity'));
		$item->setAddress($request->request->get('address'));
		
		$this->em->flush();
		
		return $this->respond($repository->find($id), 'The order has been updated successfully');
	}
	
	
	#[Route('/api/orders/delete', name: 'app_api_orders_delete', methods: ['POST'])]
	public function delete(Request $request): Response
	{
		$client = $this->decodeJWT($request, $this->em);
		$repository = $this->em->getRepository(Order::class);
		
		$item = $repository->findOneBy([
			'id' => $request->request->get('id'),
			'client' => $client,
		]);
		
		if (!$item) {
			return $this->respond($request->request->all(), 'Order not found!', Response::HTTP_BAD_REQUEST);
		}
		
		$this->em->remove($item);
		$this->em->flush();
		
		return $this->respond($item, 'Deleting the Order is successfully completed');
	}
}
