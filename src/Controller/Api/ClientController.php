<?php

namespace App\Controller\Api;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractApiController
{
	private EntityManagerInterface $em;
	
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}
	
	
    #[Route('/api/clients', name: 'app_api_clients', methods: ['GET'])]
    public function index(): Response
    {
		$repository = $this->em->getRepository(Client::class);
		
		$items = $repository->findAll();
		
		return $this->respond($items, 'Success');
    }
}
