<?php

namespace App\Controller\Api;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

class ProductController extends AbstractApiController
{
	private EntityManagerInterface $em;
	
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}
	
	
    #[Route('/api/products', name: 'app_api_products', methods: ['GET'])]
    #[Security(name: 'Bearer')]
    public function index(): Response
    {
		$repository = $this->em->getRepository(Product::class);
		
		$items = $repository->findAll();
		
		return $this->respond($items, 'Success');
    }
}
