<?php

namespace App\Controller\Api;

use App\Entity\Merchant;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MerchantController extends AbstractApiController
{
	private EntityManagerInterface $em;
	
	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}
	
	
    #[Route('/api/merchants', name: 'app_api_merchants', methods: ['GET'])]
    #[Security(name: 'Bearer')]
    public function index(): Response
    {
		$repository = $this->em->getRepository(Merchant::class);
		
		$items = $repository->findAll();
		
		return $this->respond($items, 'Success');
    }
}
