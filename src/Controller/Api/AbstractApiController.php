<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractFOSRestController
{
    protected function buildForm(string $type, $data = null, array $options = []): FormInterface
    {
        $options = array_merge($options, [
           'csrf_protection' => false,
        ]);

        return $this->container->get('form.factory')->createNamed('', $type, $data, $options);
    }

    public function respond($data, string $message, int $statusCode = Response::HTTP_OK): Response
    {
        return $this->handleView($this->view([
			'status' => $statusCode,
	        'message' => $message,
	        'data' => $data,
        ], $statusCode));
    }
	
	protected function decodeJWT(Request $request, EntityManagerInterface $em)
	{
		$split = explode(' ', $request->headers->get('authorization'));
		
		if (count($split) < 2) {
			return $this->respond(null, 'Invalid token!');
		}
		
		$token = $split[1];
		
		$tokenParts = explode(".", $token);
		$tokenHeader = base64_decode($tokenParts[0]);
		$tokenPayload = base64_decode($tokenParts[1]);
		$jwtHeader = json_decode($tokenHeader);
		$jwtPayload = json_decode($tokenPayload);
		
		if (!($jwtPayload->username ?? false)) {
			return $this->respond(null, 'Invalid token!');
		}
		
		$manager = $em->getRepository(Client::class);
		
		$client = $manager->findOneBy(['username' => $jwtPayload->username]);
		
		if (!$client) {
			return $this->respond(['username' => $jwtPayload->username], 'Invalid token!');
		}
		
		return $client;
	}
}
