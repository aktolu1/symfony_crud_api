<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220516175909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD COLUMN name VARCHAR(60) NOT NULL');
        $this->addSql('DROP INDEX IDX_F529939819EB6921');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order AS SELECT id, client_id, order_code, quantity, address, shipping_date FROM "order"');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('CREATE TABLE "order" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, client_id INTEGER NOT NULL, order_code VARCHAR(20) NOT NULL, quantity SMALLINT NOT NULL, address VARCHAR(255) DEFAULT NULL, shipping_date DATETIME DEFAULT NULL, CONSTRAINT FK_F529939819EB6921 FOREIGN KEY (client_id) REFERENCES client (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO "order" (id, client_id, order_code, quantity, address, shipping_date) SELECT id, client_id, order_code, quantity, address, shipping_date FROM __temp__order');
        $this->addSql('DROP TABLE __temp__order');
        $this->addSql('CREATE INDEX IDX_F529939819EB6921 ON "order" (client_id)');
        $this->addSql('DROP INDEX IDX_D34A04AD6796D554');
        $this->addSql('CREATE TEMPORARY TABLE __temp__product AS SELECT id, merchant_id, name FROM product');
        $this->addSql('DROP TABLE product');
        $this->addSql('CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, merchant_id INTEGER NOT NULL, name VARCHAR(128) NOT NULL, CONSTRAINT FK_D34A04AD6796D554 FOREIGN KEY (merchant_id) REFERENCES merchant (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO product (id, merchant_id, name) SELECT id, merchant_id, name FROM __temp__product');
        $this->addSql('DROP TABLE __temp__product');
        $this->addSql('CREATE INDEX IDX_D34A04AD6796D554 ON product (merchant_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_C7440455F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__client AS SELECT id, username, roles, password FROM client');
        $this->addSql('DROP TABLE client');
        $this->addSql('CREATE TABLE client (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO client (id, username, roles, password) SELECT id, username, roles, password FROM __temp__client');
        $this->addSql('DROP TABLE __temp__client');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C7440455F85E0677 ON client (username)');
        $this->addSql('DROP INDEX IDX_F529939819EB6921');
        $this->addSql('CREATE TEMPORARY TABLE __temp__order AS SELECT id, client_id, order_code, quantity, address, shipping_date FROM "order"');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('CREATE TABLE "order" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, client_id INTEGER NOT NULL, order_code VARCHAR(20) NOT NULL, quantity SMALLINT NOT NULL, address VARCHAR(255) DEFAULT NULL, shipping_date DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO "order" (id, client_id, order_code, quantity, address, shipping_date) SELECT id, client_id, order_code, quantity, address, shipping_date FROM __temp__order');
        $this->addSql('DROP TABLE __temp__order');
        $this->addSql('CREATE INDEX IDX_F529939819EB6921 ON "order" (client_id)');
        $this->addSql('DROP INDEX IDX_D34A04AD6796D554');
        $this->addSql('CREATE TEMPORARY TABLE __temp__product AS SELECT id, merchant_id, name FROM product');
        $this->addSql('DROP TABLE product');
        $this->addSql('CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, merchant_id INTEGER NOT NULL, name VARCHAR(128) NOT NULL)');
        $this->addSql('INSERT INTO product (id, merchant_id, name) SELECT id, merchant_id, name FROM __temp__product');
        $this->addSql('DROP TABLE __temp__product');
        $this->addSql('CREATE INDEX IDX_D34A04AD6796D554 ON product (merchant_id)');
    }
}
