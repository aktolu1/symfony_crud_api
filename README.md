# Symfony 5.4 CRUD API

## Installation
```sh
composer install # install dependencies

php bin/console make:migration # create database and tables

php bin/console doctrine:migrations:migrate # save changes to the DB

php bin/console doctrine:fixtures:load # sample data

```


### Generate JWT Token:

```sh
curl --request POST \
  --url http://127.0.0.1:8000/api/login_check \
  --header "Content-Type: application/json" \
  --data '{
	"username": "client_1",
	"password": "1234"
}'
```


### Api documentation
[Still working about it](http://127.0.0.1/api/doc)

### List all clients:

```sh
curl --request GET \
  --url http://127.0.0.1:8000/api/clients \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json"
```


### List Products:

```sh
curl --request GET \
  --url http://127.0.0.1:8000/api/products \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json"
```


### List Merchants:

```sh
curl --request GET \
  --url http://127.0.0.1:8000/api/merchants \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json"
```


### List Orders:

```sh
curl --request GET \
  --url http://127.0.0.1:8000/api/orders \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json"
```

### Get a Specific Order:

```sh
curl --request GET \
  --url http://127.0.0.1:8000/api/orders/10 \
  --header "Content-Type: application/json"
```


### Create Order:

```sh
curl --request POST \
  --url http://127.0.0.1:8000/api/orders/add \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json" \
  --data '{
	"quantity": "1",
	"address": "Simpsons Street",
	"product": "10"
}'
```

### Update Order:

```sh
curl --request POST \
  --url http://127.0.0.1:8000/api/orders/update/14 \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json" \
  --data '{
	"product": 9,
	"quantity": 1,
	"address": "Changed"
}'
```


### Delete Order:

```sh
curl --request POST \
  --url http://127.0.0.1:8000/api/orders/delete \
  --header "Authorization: Bearer TOKEN_WILL_BE_HERE" \
  --header "Content-Type: application/json" \
  --data '{
	"id": 15
}'
```


